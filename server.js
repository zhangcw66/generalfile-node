// express框架搭建本地服务
const express = require('express')
const server = express()
// 设置node服务监听的端口号
const port = 3000

server.listen(port, () => {
  console.log('express-serve is running...')
})

// node对于跨域的处理
server.use((req, res, next) => {
  res.header('Access-Control-Allow-Credentials', 'true')
  res.header('Access-Control-Allow-Origin', '*')
  // 允许的header类型
  res.header(
    'Access-Control-Allow-Headers',
    'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild'
  )
  // 跨域允许的请求方式
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS')
  if (req.method == 'OPTIONS') {
    res.sendStatus(200) /*让options请求快速返回*/
  } else {
    next()
  }
})

server.disable('etag')
const fs = require('fs')
const path = require('path')

server.get('/index', (req, res) => {
  // 对传递的参数进行处理，传递参数的格式是 path='D:/images/node/.....'
  if (!req.query.path) return
  var searchPath = req.query.path
  //例如：文件的路径是/mnt/lustre/yangfan1/Mydataset/kdlk_test/badcase.folder/badcase/Hack_img

  // // 图片展示列表(显示的是图片的完整路径)
  const imgList = []
  // // 存放文件的列表(显示的是图片的完整路径)
  const filesList = []
  // // 存放文件的完整路径，方便直接进行文件查找
  const completePath = []
  // // 存放图片的名称
  const imgNameList = []
  // // 存放文件名称
  const fileNameList = []
  // 判断文件是否为图片
  const imgType = /\.(jpg|jpeg|png|bmp|BMP|JPG|PNG|JPEG|jfif)$/
  let promiseList = []

  fs.stat(searchPath, function (err, stats) {
    if (err) {
      return res.json({state: 404, message: '读取该目录失败,文件不存在'})
    } else if (stats.isFile()) {
      readFile()
    } else if (stats.isDirectory()) {
      readDir()
    }
  })

  function readFile() {
    if (imgType.test(searchPath)) {
      promiseList.push(proReadFile(searchPath))
      allPromise()
    } else {
      filesList.push(searchPath)
      return res.json({
        files: filesList,
        images: imgList,
        fileName: fileNameList,
        imgName: imgNameList
      })
    }
  }

  function proReadFile(searchPath, item) {
    return new Promise((resolve, reject) => {
      fs.readFile(searchPath, function(err, files) {
        if (err) {
          return reject()
        } else {
          let bufferData = Buffer.from(files).toString('base64')
          let base64 = 'data:image/jpg;base64,' + bufferData
          // 图片base64完整信息放入图片的列表中
          imgList.push(base64)
          if (item) { imgNameList.push(item) }
          resolve()
        }
      })
    })
  }

  function readDir() {
    fs.readdir(searchPath, function(err, files) {
      if (err) {
        return (
          res.status(200) && res.json({ state: 401, message: '读取该目录失败' })
        )
      }
      const imgStr = /\.(jpg|jpeg|png|bmp|BMP|JPG|PNG|JPEG|jfif)$/
      const otherForm = /\.(txt|doc|hlp|wps|rtf|html|pdf|rar|zip|arj|gz|z|wav|aif|au|mp3|ram|wma|mmf|amr|aac|flac|int|sys|dll|adt|exe|com|c|asm|for|lib|lst|msg|obj|pas|wki|bas|map|bak|tmp|dot|bat|cmd|js|md|json|docx|pub|pptx|rtf|xlsx|zip)$/

      if (!files) {
        return res.sendStatus(404) && res.json('该文件夹不存在')
      }
      if (files && files.length === 0) {
        return (
          res.status(200) &&
          res.json({ state: 400, message: '该文件夹为空文件夹' })
        )
      }
      // 对读取的文件进行处理
      files.filter((item, index) => { // proReadFile(searchPath)
        if (imgStr.test(item)) {
          promiseList.push(proReadFile(searchPath + '/' +item, item))
        } else if (otherForm.test(item)) {
          files.splice(index, 1)
        } else if (item) {
          // 文件的完整路径
          if (new RegExp('^//+').test(searchPath)) {
            searchPath = searchPath.replace(new RegExp('^//+'), '/')
          }
          if (searchPath === '/') {
            searchPath = ''
          }
          filesList.push(searchPath + '/' + item)
          // 完整路径
          completePath.push(searchPath)
          //  文件名称
          fileNameList.push(item)
        }
      })
      //
      allPromise()
    })
  }
  function allPromise () {
    Promise.all(promiseList)
      .then(_ => {
        // 将查询结果返回进行展示；需要返回的是文件名，文件完整路径，图片完整路径，图片名
        return res.json({
          files: filesList,
          images: imgList,
          fileName: fileNameList,
          imgName: imgNameList
        })
      })
      .catch(err => {
        console.log(err)
        // 将查询结果返回进行展示；需要返回的是文件名，文件完整路径，图片完整路径，图片名
        return res.json({
          files: filesList,
          images: imgList,
          fileName: fileNameList,
          imgName: imgNameList
        })
      })
  }
})
